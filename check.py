complex_numbers = set()

i = -2
while i < 2:
    j = -2
    while j < 2:
        complex_numbers.add(complex(i, j))
        j += 0.001
    i += 0.001

complex_numbers_not_found = []
        
with open('out.txt', 'r') as f:
    for line in f.readlines():
        temp = line.split(' ')
        c = complex(float(temp[0]), float(temp[1]))

        if c in complex_numbers:
            complex_numbers.remove(c)
        else:
            complex_numbers_not_found.append(c)

with open('not_found.txt', 'w') as f:
    for c in complex_numbers_not_found:
        f.write(f'{c.real} {c.imag} \n')
