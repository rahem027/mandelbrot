#include "mandelbrot.hpp"
#include <unordered_set>
#include <iostream>
#include <vector>
#include <future>
#include <cmath>
#include <chrono>
#include <cassert>


/**
 * Check if Complex c is infinite.
 * If c is infinite, infinite - infinite != 0
 * If c is nan, nan - nan != 0
 */

bool infinite(Complex c) {
	return c - c != Complex{};
}


/**
 * A class that can add members to a datastructure that only stores
 * unique values. Created to benchmark different data structures for
 * this particular use case. 
 */

class unique_set {
	std::unordered_set<Complex, complex_hasher> set{};
	// std::vector<Complex> set;
  
public:

	unique_set(){}

    
	/**
	 * Add element to data structure if not already exists. 
	 * Return if the element was inserted. False otherwise
	 */
    
	bool add(Complex c) {
		auto [_, inserted] = set.insert(c);
		return inserted;

		// if (std::find(set.cbegin(), set.cend(), c) == set.cend()) {
		// 	set.push_back(c);
		// 	return true;
		// }
		
		// return false;
	}
};


/**
 * The number of iterations it takes function f to output
 * a value twice. F is successively called on its own 
 * output. Ex - f(x), f(f(x)), f(f(f(x))), etc. till max_iteration
 * if no value is repeated till max iterations, empty optional 
 * is retuned. Otherwise the number of iterations it took for a 
 * value to repeat is returned. 
 *
 * !!Note:!! The value will never be less than 2 if the function
 * repeats. This is because we need at least two values to check 
 * if a value is repeated. 
 */

std::optional<int> iterations_to_repeat(std::function<Complex(Complex)> f,
										int max_iterations) {
	Complex x{};
  
	unique_set outputs;
    
	int iterations = 0;
  
	while (iterations < max_iterations) {
		iterations++;
		x = f(x);

		bool inserted = outputs.add(x);
		bool repeated = !inserted;

		if (repeated) {
			return iterations;
		}

		if (infinite(x)) {
			return {};
		}
	}

	return {};
}


/**
 * Returns a map of complex number and an optional int 
 * indicating the number of iterations it took to reach 
 * infinity. If the function does not reach infinity within
 * max_iterations, the function is deemed infinite and empty
 * optional is returned as the value
 */

MandelbrotSet _mandelbrot_set(HalfOpenRange real_range,
							  HalfOpenRange imag_range,
							  T step,
							  int max_iterations) {
	// std::cout << "real start: " << real_range.start << '\n'
	//   		  << "real end: " << real_range.end << '\n'
	// 		  << "step: " << step << '\n'
	// 		  << "max iterations: " << max_iterations << '\n';;
	// auto start = std::chrono::system_clock::now();
    MandelbrotSet result;

    T real = real_range.start;

    while (real < real_range.end) {
		T imag = imag_range.start;

		while (imag < imag_range.end) {
			const Complex c(real, imag);
	    
			auto mandelbrot = [&](Complex x) {
				return x * x + c;
			};
	    
			auto iterations = iterations_to_repeat(mandelbrot, max_iterations);
			result[c] = iterations;
	    
			imag += step;
		}

		real += step;
    }
	
	// auto end = std::chrono::system_clock::now();

	// 
	// 		  << "time taken: " << (end - start).count() << '\n';

    return result;
}


T round_first_param_to_second(T first, T second) {
	if (first == 0) {
		return 0;
	}
	
	T quotient = std::ceil(first / second);
	
	bool parameters_are_divisible = quotient == first / second;
	if (parameters_are_divisible) {
		return first;
	}
	
	return second * (quotient + 1);
}


class CPULoadBalancer {
	/**
	 * The real_range and imag_range within which we have to find 
	 * mandelbrot set.
	 */
	const HalfOpenRange real_range, imag_range;

	/**
	 * The number with which we want to increment [real|imag]_range
	 * after each loop.
	 * 
	 * Passed directly to _mandelbrot_set
	 */
	const T step;


	/**
	 * Number of steps to execute at a time
	 */
	const int batch_size;

	/**
	 * Max iterations to execute before declaring the function does
	 * not repeat.
	 *
	 * Passed directly to _mandelbrot_set
	 */
	const int max_iterations;


	/**
	 * Number of CPU threads to use
	 */
	const size_t cpu_threads;


	/**
	 * Vector containing executing threads.
	 */
	std::vector<std::thread> threads;


	/**
	 * Unordered map containing the result. Order of result cannot
	 * be specified due to thread scheduling and some ranges of numbers
	 * takes more time to compute than others.
	 */
	MandelbrotSet result;

	
	/**
	 * Next thread to execute should start from this value
	 */
	T real_start;


	/**
	 * Mutex to prevent race conditions against real_start
	 */
	std::mutex real_start_mutex;

	
	/**
	 * Mutex to prevent race conditions against real_start
	 */
	std::mutex result_mutex;
public:

	/**
	 * Create the load balancer and start executing
	 */
	CPULoadBalancer(
		HalfOpenRange _real_range,
		HalfOpenRange _imag_range,
		T _step,
		int _max_iterations,
		size_t _cpu_threads,
		int _batch_size = 100
	)
		: // Values just being copied from input
		  real_range(_real_range),
		  imag_range(_imag_range),
		  step(_step),		  
		  batch_size(_batch_size),
		  max_iterations(_max_iterations),
		  cpu_threads(_cpu_threads),		  

		  // thread is a vector. I am giving it cpu_threads as the
		  // size arguments for the vector constructor
		  threads(_cpu_threads),
		  // current_real_start is initialized to real_range.start
		  real_start(_real_range.start) {

		assert(batch_size > 0);
		
	    for (auto& thread : threads) {
			auto work = [&] {
				T current_real_start;
				T current_real_end;

				// not an infinite loop. The exit condition is inside the loop
				while (true) {
					// Additional scope created to ensure mutex is released asap
					// Without it, the program will be slower than synchronous
					// code and there will be no benefit of using threads
					// because most of the time will be spent waiting for
					// mutexes.
					{
						std::lock_guard<std::mutex> guard(real_start_mutex);
						if (real_start >= real_range.end) {
							return;
						}

						current_real_start = real_start;
					
						/**
						 * batch_size is number of steps to execute at a time. 
						 * so we mulitply it with steps to get the number we
						 * need to add.
						 *
						 * We need estimated end because the last thread 
						 * might get less work than usual;
						 */
						T estimated_end = real_start + (step * batch_size);
						current_real_end = std::min(real_range.end, estimated_end);										

						// next thread should start from current_real_end
						// this works because the range is half open
						real_start = current_real_end;
					}

					const auto mandelbrot = _mandelbrot_set(
															HalfOpenRange(current_real_start, current_real_end),
															imag_range,
															step,
															max_iterations
															);


					// no scope created because the block is about to
					// end anyway
					std::lock_guard<std::mutex> guard(result_mutex);
					result.insert(mandelbrot.cbegin(), mandelbrot.cend());
				}

				
			};
			
			thread = std::thread(work);
		}
	}


	/**
	 * Wait for result from all threads
	 */
	MandelbrotSet wait_for_result() {
		for (auto& thread : threads) {
			thread.join();
		}

		return result;
	}
};


MandelbrotSet mandelbrot_set(
	HalfOpenRange real_range,
	HalfOpenRange imag_range,
	T step,
	int max_iterations,
	size_t thread_count
) {
    /// Thread count may be 0 if implementation cannot
    /// find thread count. So use one thread in that case.    
    if (thread_count < 1)
		thread_count = 1;

	CPULoadBalancer cpu_load_balancer(
		real_range,
		imag_range,
		step,
		max_iterations,
		thread_count
	);	

    return cpu_load_balancer.wait_for_result();
}
