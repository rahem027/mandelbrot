#include "stfu.h"
#include "mandelbrot.hpp"

int main() {
	using namespace stfu;
	
	auto runner = test("Iterations to repeat test cases", [] {
		test("f(x) = x^2 + (-1 - 1i) takes 3 iterations to repeat", [] {
			const Complex c(-1);
			auto mandelbrot_set = [=](Complex x) {
				return x * x + c;
			};

			int iterations = iterations_to_repeat(mandelbrot_set, 1000).value();

			/**
			 * In iteration 1: x = f(x) => -1
			 * In iteration 2: x = f(x) => 0
			 * In iteration 3: x = f(x) => -1
			 */
			assert(iterations == 3);
		});

		test("f(x) = 1 takes 2 iterations to repeat", [] {
			auto unity = [](Complex c) {
				return c * Complex() + Complex(1);
			};

			int iterations = iterations_to_repeat(unity, 1000).value();

			assert(iterations == 2);
		});

		test("f(x) = x^2 + 1 returns empty optional because the elements never repeat", [] {
			const Complex c(1);
			auto mandelbrot_set = [=](Complex x) {
				return x * x + c;
			};

			bool has_value = iterations_to_repeat(mandelbrot_set, 100).has_value();

			assert(has_value == false);
		});
		
	});

	runner();

	runner = test("Mandelbrot set test cases", [] {
		test("Mandelbrot set between -1 - i and 1 + i with step 1 "
			 "returns the correct result", [] {

				 /// HalfOpenRanges loop over start to end - 1
				 HalfOpenRange real_range(-1, 2);
				 HalfOpenRange imag_range(-1, 2);

				 auto result = mandelbrot_set(real_range, imag_range, 1);
				 print(result);
				 assert(result.size() == 9);

				 /// Complex numbers that don't repeat
				 assert(result[Complex(-1, -1)].has_value() == false);
				 assert(result[Complex(-1, 1)].has_value() == false);
				 assert(result[Complex(1, 0)].has_value() == false);
				 assert(result[Complex(1, -1)].has_value() == false);
				 assert(result[Complex(1, 0)].has_value() == false);
				 assert(result[Complex(1, 1)].has_value() == false);
				 
				 /// Complex numbers that repeat
				 assert(result[Complex(-1, 0)].value() == 3);
				 assert(result[Complex(0, -1)].value() == 4);
				 assert(result[Complex(0, 0)].value() == 2);
				 assert(result[Complex(0, 1)].value() == 4);
			 });
	});

	runner();
}
