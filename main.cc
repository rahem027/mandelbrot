#include <fstream>
#include <sstream>

#include "mandelbrot.hpp"

int main(int argc, char** argv) {
	if (argc != 6) {
		std::cout << "Invalid arguments. \n"
				  << "Usage: " << argv[0] 
				  << " <real start> <real end>"
				  << " <imag start> <imag end>"
				  << " <step count>\n";
		return 0;		
	}

	std::stringstream ss;
	ss << argv[1] << ' '
	   << argv[2] << ' '
	   << argv[3] << ' '
	   << argv[4] << ' '
	   << argv[5];
	
	T real_start, real_end, imag_start, imag_end, step;
	
	ss >> real_start >> real_end >> imag_start >> imag_end >> step;
	
	HalfOpenRange real_range(real_start, real_end);
	HalfOpenRange imag_range(imag_start, imag_end);

	auto result = mandelbrot_set(real_range, imag_range, step);

	std::cout << "Result size: " << result.size() << '\n';
	
	std::ofstream file("out.txt");
	
	for (auto& pair : result) {
		file << pair.first.real() << ' ' << pair.first.imag() << ' '
			 << pair.second.value_or(-1) << '\n';
	}
	
	return 0;
}
